const root = location.protocol+'//'+location.host+location.pathname.replace(/[^/]*$/, '');

var visibilityChanger = function(element_id) {
    return function(visible) {
	document.getElementById(element_id).style.display = visible ? 'inline' : 'none';
    }
}

var pdfjs = function(url) {
    let frame = document.createElement("iframe");
    frame.src = url;
    document.getElementById("pdfjs").appendChild(frame);
}

var appendOutput = function(msg) {
    var content = document.getElementById("output").textContent;

    var output = document.getElementById("output");
    output.textContent = content + "\r\n" + msg;

    var wrapper = document.getElementById("output_wrapper");
    wrapper.scrollTop = wrapper.scrollHeight;
}

var pdf_dataurl = undefined;
var compile = function(source_code) {
    var texlive = new TeXLive("texlive.js/");
    var pdftex = texlive.pdftex;
    pdftex.on_stdout = appendOutput;
    pdftex.on_stderr = appendOutput;

    var start_time = new Date().getTime();

    pdftex.FS_createPath('', 'images', /*canRead=*/true, /*canWrite=*/true).then(function() {
	pdftex.FS_createLazyFile('images', 'arch_logo.png', root + 'latex-source/images/arch_logo.png', /*canRead=*/true, /*canWrite=*/true).then(function() {
	    pdftex.compile(source_code).then(function(pdf_dataurl) {
		var end_time = new Date().getTime();
		appendOutput("Execution time: " + (end_time - start_time) / 1000 + ' sec');

		if (pdf_dataurl === false)
		    return;
		texlive.terminate();
		pdfjs("https://shizcow.gitlab.io/resume/pdfjs/web/viewer.html#" + pdf_dataurl);
	    });
	});
    });
}

var download_and_compile = function() {
    appendOutput("Downloading " + root + 'latex-source/main.tex');
    fetch(root + 'latex-source/main.tex').then(function(response) {
	return response.text().then(function(source_code) {
	    appendOutput("Downloaded main.tex");
	    compile(source_code);
	});
    });
}

window.onload = download_and_compile;
