default:
	@echo "Building resume locally..."
	cd latex-source && pdflatex main.tex

test:	default
	okular latex-source/main.pdf
