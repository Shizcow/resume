# Resume

My seriously over-engineered resume.

| Objectives |
| ---- |
| <ul><li>[x] Written in latex with opencv</li></ul> |
| <ul><li>[ ] Dynamically updates using Rust compiled to webassembly</li></ul> |
| <ul><li>[x] Compiles in real time via a custom build of texlive.js</li></ul> |
| <ul><li>[x] Rendered using pdf.js</li></ul> |
| <ul><li>[ ] Tied together with Rocket.rs</li></ul> |
